require asyn, 4.33.0
require stream, 2.8.8
require prempScanAnalog, 0.0.1


# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("ASYN_PORT",      "sad-plc-premp-001")
epicsEnvSet("IP_ADDR",        "172.30.244.59")
epicsEnvSet("PORT_ADDR",      "5000")
epicsEnvSet("PREFIX",         "SES-PREMP:ANALOG-01")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(prempScanAnalog_DIR)db/")

iocshLoad("$(prempScanAnalog_DIR)prempScanAnalog.iocsh", "ASYN_PORT=$(ASYN_PORT), IP_ADDR=$(IP_ADDR), PORT_ADDR=$(PORT_ADDR), PREFIX=$(PREFIX)")

# Start the IOC
iocInit
